set linebreak
set nowrap 
set ruler 
set nu 
set noswapfile 
set mouse=a 
set cc=81
set tabstop=4
set shiftwidth=4
set nocompatible
syntax on
set showmatch
colo delek
filetype plugin indent off

if !1 | finish | endif

if empty(glob('~/.vim/autoload/plug.vim'))
    echo "Installing VimPlug..."
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'pandark/42header.vim'
call plug#end()
set laststatus=2
set noshowmode
set background=dark
colorscheme palenight
if (has("termguicolors"))
  set termguicolors
endif
let g:palenight_terminal_italics=1
let g:lightline = {
      \ 'colorscheme': 'palenight',
      \ }

map <C-b> :NERDTreeToggle<CR>
augroup numbertoggle
  autocmd!
  autocmd InsertEnter * set relativenumber
  autocmd InsertLeave   * set norelativenumber
augroup END
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
nmap <C-f1> :FortyTwoHeader<CR>
nnoremap <C-j> :tabprevious<CR>
nnoremap <C-k> :tabnext<CR>
