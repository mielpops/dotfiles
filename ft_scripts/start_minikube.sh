rm -rf ~/.minikube
mkdir -p "/Volumes/Storage/goinfre/$USER/.minikube"
ln -s mkdir "/Volumes/Storage/goinfre/$USER/.minikube" ~/.minikube
minikube start --vm-driver=docker --extra-config=apiserver.service-node-port-range=1-35000
